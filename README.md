# cringfetch
As seen on Linux!

[![Project Status: Moved to https://git.sr.ht/~mkware/mkrs – The project has been moved to a new location, and the version at that location should be considered authoritative.](https://www.repostatus.org/badges/latest/moved.svg)](https://www.repostatus.org/#moved) to [https://git.sr.ht/~mkware/cringfetch](https://git.sr.ht/~mkware/cringfetch)

***

cringfetch does what neofetch/screenfetch/\*fetch does. It shows short info about your system, but now on Windows!

***

![screenshot](https://i.imgur.com/QZLTesm.png)

## Colors

You can set your custom color of Windows logo and labels. Check `Program.cs` for details.

To see which colors are available, check `cmdColors.png` or `PSColors.png`.

# Installation

## Using

(Requires .NET Framework 4)

1. [sourcehut] Get MKware built binaries at [https://mkware.eu.org/dc](https://mkware.eu.org/dc)
2. Place it wherever you want
3. Run `cmd.exe`
4. Run program from cmd

If you want to run cringfetch from any place, copy `cringfetch.exe` to your `%windir%`.

## Source code

1. Download zip archive with source code.
2. Open `cringfetch.sln` in Visual Studio (2013 recommended)

## Compiling

1. Click `Build` from top bar menu
2. Click `Build solution`
3. Get built binary in `cringfetch\bin\Debug` or `cringfetch\bin\Release`
